package fr.gheizmann.marmimiam.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.gheizmann.marmimiam.dao.UtilisateurDao;
import fr.gheizmann.marmimiam.model.user.Utilisateur;

@Service
public class MyUserDetailsService implements UserDetailsService {
	private UtilisateurDao utilisateurDao;
	
	@Autowired
	public MyUserDetailsService(
			final UtilisateurDao utilisateurDao) {
		this.utilisateurDao = utilisateurDao;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Utilisateur utilisateur = utilisateurDao.findByLogin(username)
				.orElseThrow(() -> new UsernameNotFoundException("Utilisateur inconnu : " + username));
		return new MyUserDetails(utilisateur);
	}
}
