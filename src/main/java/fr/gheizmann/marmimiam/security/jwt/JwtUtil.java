package fr.gheizmann.marmimiam.security.jwt;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtUtil {
	@Value("${jwt.secret}")
	private String secret;
	
	public Claims extractionDuCorpsDuToken(String token) {
		return Jwts.parser()
				.setSigningKey(secret)
				.parseClaimsJws(token)
				.getBody();
	}
	
	public String gerenerToken(UserDetails userDetails) {
		Map<String, Object> tokenData = new HashMap<>();
		
		tokenData.put("roles", userDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(",")));
		
		return Jwts.builder()
				.setClaims(tokenData)
				.setSubject(userDetails.getUsername())
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000*60*45))
				.signWith(SignatureAlgorithm.HS256, secret).compact();
	}
	
	public Boolean tokenNonDepasseDateExpiration(String token) {
		return extractionDuCorpsDuToken(token)
				.getExpiration()
				.before(new Date());
	}
	
	public Boolean validationToken(String token, UserDetails userDetails) {
		final String username = extractionDuCorpsDuToken(token).getSubject();
		
		return (username.equals(userDetails.getUsername()) && !tokenNonDepasseDateExpiration(token));
	}
}
