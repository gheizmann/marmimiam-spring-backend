package fr.gheizmann.marmimiam.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.Categorie;

@Repository
public interface CategorieDao extends JpaRepository<Categorie, Integer> {
	Optional<Categorie> findByNom(String nom);
}
