package fr.gheizmann.marmimiam.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.IngredientRecette;
import fr.gheizmann.marmimiam.model.IngredientRecetteKey;

@Repository
public interface IngredientRecetteDao extends JpaRepository<IngredientRecette, IngredientRecetteKey> {
	List<IngredientRecette> findByRecetteId(Integer recetteId);
}
