package fr.gheizmann.marmimiam.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.Recette;

@Repository
public interface RecetteDao extends JpaRepository<Recette, Integer> {	
	List<Recette> findByUtilisateurId(Integer utilisateurId);
}
