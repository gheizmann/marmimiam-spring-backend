package fr.gheizmann.marmimiam.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.Notation;
import fr.gheizmann.marmimiam.model.NotationKey;

@Repository
public interface NotationDao extends JpaRepository<Notation, NotationKey> {
	List<Notation> findByRecetteId(Integer recetteId);
	Optional<Notation> findByUtilisateurIdAndRecetteId(Integer utilisateurId, Integer recetteId);
}
