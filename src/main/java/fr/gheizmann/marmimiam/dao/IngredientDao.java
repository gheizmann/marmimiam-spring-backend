package fr.gheizmann.marmimiam.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.Ingredient;

@Repository
public interface IngredientDao extends JpaRepository<Ingredient, Integer> {
	Optional<Ingredient> findByNom(String nom);
}
