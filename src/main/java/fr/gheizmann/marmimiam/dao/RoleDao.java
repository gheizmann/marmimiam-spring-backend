package fr.gheizmann.marmimiam.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.user.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer> {
	Optional<Role> findByNom(String nom);
}
