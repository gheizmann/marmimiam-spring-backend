package fr.gheizmann.marmimiam.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.gheizmann.marmimiam.model.user.Utilisateur;

@Repository
public interface UtilisateurDao extends JpaRepository<Utilisateur, Integer> {
	Optional<Utilisateur> findByLogin(String login);
}
