package fr.gheizmann.marmimiam.dto;

import java.util.List;

public class RecetteDto {
	private String titre;
	private String categorie;
	private String urlImage;
	private Integer dureePreparation;
	private Integer dureeCuisson;
	private int difficulte;
	private int prix;
	private String texte;
	private List<IngredientRecetteDto> listeIngredients;
	
	public String getTitre() {
		return titre;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public Integer getDureePreparation() {
		return dureePreparation;
	}
	
	public void setDureePreparation(Integer dureePreparation) {
		this.dureePreparation = dureePreparation;
	}
	
	public Integer getDureeCuisson() {
		return dureeCuisson;
	}
	
	public void setDureeCuisson(Integer dureeCuisson) {
		this.dureeCuisson = dureeCuisson;
	}
	
	public int getDifficulte() {
		return difficulte;
	}
	
	public void setDifficulte(int difficulte) {
		this.difficulte = difficulte;
	}
	
	public int getPrix() {
		return prix;
	}
	
	public void setPrix(int prix) {
		this.prix = prix;
	}
	
	public String getTexte() {
		return texte;
	}
	
	public void setTexte(String texte) {
		this.texte = texte;
	}
	
	public List<IngredientRecetteDto> getListeIngredients() {
		return listeIngredients;
	}
	
	public void setListeIngredients(List<IngredientRecetteDto> listeIngredients) {
		this.listeIngredients = listeIngredients;
	}
}
