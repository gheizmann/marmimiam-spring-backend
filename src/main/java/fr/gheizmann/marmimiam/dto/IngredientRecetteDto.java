package fr.gheizmann.marmimiam.dto;

import fr.gheizmann.marmimiam.model.UniteMesure;

public class IngredientRecetteDto {
	private String nom;
	private Integer quantite;
	private UniteMesure unite;
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Integer getQuantite() {
		return quantite;
	}
	
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}
	
	public UniteMesure getUnite() {
		return unite;
	}
	
	public void setUnite(UniteMesure unite) {
		this.unite = unite;
	}
}
