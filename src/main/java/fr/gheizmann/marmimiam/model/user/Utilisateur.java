package fr.gheizmann.marmimiam.model.user;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "utilisateur")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Utilisateur {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private int id;
	@Column(nullable = false, unique = true)
	private String login;
	@Column(nullable = false, length = 60)
	@JsonIgnore
	private String password;
	@JsonIgnore
	private boolean active;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "utilisateur_role",
	joinColumns = @JoinColumn(name = "utilisateur_id"),
	inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> listeRole;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Set<Role> getListeRole() {
		return listeRole;
	}
	
	public void setListeRole(Set<Role> listeRole) {
		this.listeRole = listeRole;
	}
}
