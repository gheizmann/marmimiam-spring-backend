package fr.gheizmann.marmimiam.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.gheizmann.marmimiam.model.user.Utilisateur;

@Table(name = "recette")
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Recette {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String titre;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date dateMiseEnLigne;
	@ManyToOne
	@JoinColumn(name = "categorie_id")
	private Categorie categorie;
	private String urlImage;
	private Integer dureePreparation;
	private Integer dureeCuisson;
	private int difficulte;
	private int prix;
	@Column(nullable = false, columnDefinition = "TEXT")
	private String texte;
	@ManyToOne
	@JoinColumn(name = "utilisateur_id")
	private Utilisateur utilisateur;
	@OneToMany(mappedBy = "recette")
	private Set<IngredientRecette> listeIngredient = new HashSet<>();
	private boolean active;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Date getDateMiseEnLigne() {
		return dateMiseEnLigne;
	}

	public void setDateMiseEnLigne(Date dateMiseEnLigne) {
		this.dateMiseEnLigne = dateMiseEnLigne;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	
	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public String getTitre() {
		return titre;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public Integer getDureePreparation() {
		return dureePreparation;
	}
	
	public void setDureePreparation(Integer dureePreparation) {
		this.dureePreparation = dureePreparation;
	}
	
	public Integer getDureeCuisson() {
		return dureeCuisson;
	}
	
	public void setDureeCuisson(Integer dureeCuisson) {
		this.dureeCuisson = dureeCuisson;
	}
	
	public int getDifficulte() {
		return difficulte;
	}
	
	public void setDifficulte(int difficulte) {
		this.difficulte = difficulte;
	}
	
	public int getPrix() {
		return prix;
	}
	
	public void setPrix(int prix) {
		this.prix = prix;
	}
	
	public String getTexte() {
		return texte;
	}
	
	public void setTexte(String texte) {
		this.texte = texte;
	}
	
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Set<IngredientRecette> getListeIngredient() {
		return listeIngredient;
	}

	public void setListeIngredient(Set<IngredientRecette> listeIngredient) {
		this.listeIngredient = listeIngredient;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
