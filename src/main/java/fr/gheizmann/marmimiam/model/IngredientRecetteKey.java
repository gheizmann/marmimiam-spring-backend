package fr.gheizmann.marmimiam.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class IngredientRecetteKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7421990404323828274L;

	@Column(name = "recette_id")
	private Integer recetteId;
	@Column(name = "ingredient_id")
	private Integer ingredientId;
	
	public Integer getRecetteId() {
		return recetteId;
	}
	
	public void setRecetteId(Integer recetteId) {
		this.recetteId = recetteId;
	}
	
	public Integer getIngredientId() {
		return ingredientId;
	}
	
	public void setIngredientId(Integer ingredientId) {
		this.ingredientId = ingredientId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ingredientId == null) ? 0 : ingredientId.hashCode());
		result = prime * result + ((recetteId == null) ? 0 : recetteId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof IngredientRecetteKey)) {
			return false;
		}
		IngredientRecetteKey other = (IngredientRecetteKey) obj;
		if (ingredientId == null) {
			if (other.ingredientId != null) {
				return false;
			}
		} else if (!ingredientId.equals(other.ingredientId)) {
			return false;
		}
		if (recetteId == null) {
			if (other.recetteId != null) {
				return false;
			}
		} else if (!recetteId.equals(other.recetteId)) {
			return false;
		}
		return true;
	}
}
