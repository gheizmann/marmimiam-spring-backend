package fr.gheizmann.marmimiam.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import fr.gheizmann.marmimiam.model.user.Utilisateur;

@Table(name = "notation")
@Entity
@EntityListeners(AuditingEntityListener.class)
@IdClass(NotationKey.class)
public class Notation {
	@Id
	private Integer recetteId;
	@Id
	private Integer utilisateurId;
	@ManyToOne
	@MapsId("recette_id")
	@JoinColumn(name = "recette_id")
	private Recette recette;
	@ManyToOne
	@MapsId("utilisateur_id")
	@JoinColumn(name = "utilisateur_id")
	private Utilisateur utilisateur;
	private int note;
	@Column(columnDefinition = "TEXT")
	private String commentaire;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date dateNotation;
	
	public Integer getRecetteId() {
		return recetteId;
	}
	
	public void setRecetteId(Integer recetteId) {
		this.recetteId = recetteId;
	}
	
	public Integer getUtilisateurId() {
		return utilisateurId;
	}
	
	public void setUtilisateurId(Integer utilisateurId) {
		this.utilisateurId = utilisateurId;
	}
	
	public Recette getRecette() {
		return recette;
	}
	
	public void setRecette(Recette recette) {
		this.recette = recette;
	}
	
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	public int getNote() {
		return note;
	}
	
	public void setNote(int note) {
		this.note = note;
	}
	
	public String getCommentaire() {
		return commentaire;
	}
	
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	
	public Date getDateNotation() {
		return dateNotation;
	}
	
	public void setDateNotation(Date dateNotation) {
		this.dateNotation = dateNotation;
	}
}
