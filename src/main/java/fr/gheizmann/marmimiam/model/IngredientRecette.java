package fr.gheizmann.marmimiam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "ingredient_recette")
@Entity
@EntityListeners(AuditingEntityListener.class)
@IdClass(IngredientRecetteKey.class)
public class IngredientRecette {
	@Id
	private Integer recetteId;
	@Id
	private Integer ingredientId;
	@ManyToOne
	@MapsId("recette_id")
	@JoinColumn(name = "recette_id")
	@JsonIgnore
	private Recette recette;
	@ManyToOne
	@MapsId("ingredient_id")
	@JoinColumn(name = "ingredient_id")
	private Ingredient ingredient;
	private Integer quantite;
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 16)
	private UniteMesure unite;
	
	public Integer getRecetteId() {
		return recetteId;
	}
	
	public void setRecetteId(Integer recetteId) {
		this.recetteId = recetteId;
	}
	
	public Integer getIngredientId() {
		return ingredientId;
	}
	
	public void setIngredientId(Integer ingredientId) {
		this.ingredientId = ingredientId;
	}
	
	public Recette getRecette() {
		return recette;
	}
	
	public void setRecette(Recette recette) {
		this.recette = recette;
	}
	
	public Ingredient getIngredient() {
		return ingredient;
	}
	
	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}
	
	public Integer getQuantite() {
		return quantite;
	}
	
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}
	
	public UniteMesure getUnite() {
		return unite;
	}
	
	public void setUnite(UniteMesure unite) {
		this.unite = unite;
	}
}
