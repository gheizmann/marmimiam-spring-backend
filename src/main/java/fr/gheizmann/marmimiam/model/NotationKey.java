package fr.gheizmann.marmimiam.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NotationKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3460622903039117747L;
	
	@Column(name = "recette_id")
	private Integer recetteId;
	@Column(name = "utilisateur_id")
	private Integer utilisateurId;
	
	public Integer getRecetteId() {
		return recetteId;
	}
	
	public void setRecetteId(Integer recetteId) {
		this.recetteId = recetteId;
	}
	
	public Integer getUtilisateurId() {
		return utilisateurId;
	}
	
	public void setUtilisateurId(Integer utilisateurId) {
		this.utilisateurId = utilisateurId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((recetteId == null) ? 0 : recetteId.hashCode());
		result = prime * result + ((utilisateurId == null) ? 0 : utilisateurId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof NotationKey)) {
			return false;
		}
		NotationKey other = (NotationKey) obj;
		if (recetteId == null) {
			if (other.recetteId != null) {
				return false;
			}
		} else if (!recetteId.equals(other.recetteId)) {
			return false;
		}
		if (utilisateurId == null) {
			if (other.utilisateurId != null) {
				return false;
			}
		} else if (!utilisateurId.equals(other.utilisateurId)) {
			return false;
		}
		return true;
	}
}
