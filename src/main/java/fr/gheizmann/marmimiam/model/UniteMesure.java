package fr.gheizmann.marmimiam.model;

public enum UniteMesure {
	KILOGRAMME("kg"),
	GRAMME("g"),
	MILLIGRAMME("mg"),
	LITRE("L"),
	DECILITRE("dL"),
	CENTILITRE("cL"),
	MILLILITRE("mL"),
	PINCEE("pincée"),
	SANS_UNITE(""),
	CUILLERE_SOUPE("cuil. à soupe"),
	CUILLERE_CAFE("cuil. à café");
	
	private String unite;
	
	UniteMesure(String unite) {
		this.unite = unite;
	}
	
	public String get() {
		return unite;
	}
}
