package fr.gheizmann.marmimiam.controller.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.gheizmann.marmimiam.model.UniteMesure;

@CrossOrigin
@RestController
public class UniteMesureRestController {
	@GetMapping("/recette/liste-unites")
	public Map<String, String> getListeUnites() {
		Map<String, String> ret = new HashMap<>();
		
		for(UniteMesure unite: UniteMesure.values()) {
			ret.put(unite.name(), unite.get());
		}
		
		return ret;
	}
}
