package fr.gheizmann.marmimiam.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gheizmann.marmimiam.dto.UtilisateurDto;
import fr.gheizmann.marmimiam.security.MyUserDetailsService;
import fr.gheizmann.marmimiam.security.jwt.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

@CrossOrigin
@RestController
public class SignInRestController {
	private AuthenticationManager authenticationManager;
	private MyUserDetailsService userDetailsService;
	private JwtUtil jwtUtil;
	
	@Autowired
	public SignInRestController(
			final AuthenticationManager authenticationManager,
			final MyUserDetailsService userDetailsService,
			final JwtUtil jwtUtil) {
		this.authenticationManager = authenticationManager;
		this.userDetailsService = userDetailsService;
		this.jwtUtil = jwtUtil;
	}
	
	@GetMapping("/isvalidtoken/{token}")
	public Boolean isGoodJwt(@PathVariable String token) {
		Boolean ret = false;
		
		try {
			ret = !jwtUtil.tokenNonDepasseDateExpiration(token);
		}
		catch (SignatureException e) {
			System.out.println("Signature non valide !");
		}
		catch (ExpiredJwtException e) {
			System.out.println("Token limite temps dépassé : " + e);
		}
		catch (MalformedJwtException e) {
			System.out.println("Token mal formé : " + e);
		}
		
		return ret;
	}
	
	@PostMapping("/signin")
	public ResponseEntity<String> signin(@RequestBody UtilisateurDto utilisateurDto) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							utilisateurDto.getLogin(),
							utilisateurDto.getPassword()));
		}
		catch (BadCredentialsException e) {
			throw new Exception("Pseudo ou mot de passe incorrect");
		}
		
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(utilisateurDto.getLogin());
				
		return ResponseEntity.ok(jwtUtil.gerenerToken(userDetails));
	}
}
