package fr.gheizmann.marmimiam.controller.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class HelloTestRestController {
	@GetMapping("/hello")
	public String helloTestAll() {
		return "Hello all!";
	}
	
	@GetMapping("/user/hello")
	public String userHelloTest() {
		return "Hello user!";
	}
	
	@GetMapping("/admin/hello")
	public String adminHelloTest() {
		return "Hello admin!";
	}
}
