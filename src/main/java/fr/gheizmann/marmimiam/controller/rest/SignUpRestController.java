package fr.gheizmann.marmimiam.controller.rest;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gheizmann.marmimiam.dao.RoleDao;
import fr.gheizmann.marmimiam.dao.UtilisateurDao;
import fr.gheizmann.marmimiam.dto.UtilisateurDto;
import fr.gheizmann.marmimiam.model.user.Role;
import fr.gheizmann.marmimiam.model.user.Utilisateur;

@CrossOrigin
@RestController
public class SignUpRestController {
	private RoleDao roleDao;
	private UtilisateurDao utilisateurDao;
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	public SignUpRestController(
			final RoleDao roleDao,
			final UtilisateurDao utilisateurDao,
			final PasswordEncoder passwordEncoder) {
		this.roleDao = roleDao;
		this.utilisateurDao = utilisateurDao;
		this.passwordEncoder = passwordEncoder;
	}
	
	@PutMapping("/signup")
	public ResponseEntity<String> signup(@RequestBody UtilisateurDto utilisateurDto) throws Exception {
		ResponseEntity<String> ret;
		
		Optional<Role> optRole = roleDao.findByNom("ROLE_USER");
		Optional<Utilisateur> optUtilisateur = utilisateurDao.findByLogin(utilisateurDto.getLogin());
		Utilisateur nouveauUtilisateur;
		Set<Role> listeRole;
		if(optRole.isEmpty()) {
			throw new Exception("Erreur ! role.nom ne contient pas ROLE_USER !");
		}
		else if(optUtilisateur.isPresent()) {
			ret = ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(
					"Impossible de créer un nouvel utilisateur : l'utilisateur "
			+ utilisateurDto.getLogin() + " existe déjà !");
		}
		else {
			nouveauUtilisateur = new Utilisateur();
			nouveauUtilisateur.setLogin(utilisateurDto.getLogin());
			nouveauUtilisateur.setPassword(passwordEncoder.encode(utilisateurDto.getPassword()));
			nouveauUtilisateur.setActive(true);
			listeRole = new HashSet<>();
			listeRole.add(optRole.get());
			nouveauUtilisateur.setListeRole(listeRole);
			utilisateurDao.save(nouveauUtilisateur);
			ret = ResponseEntity.ok(
					"L'utilisateur " + utilisateurDto.getLogin() + " a été créé");
		}
		
		return ret;
	}
}
