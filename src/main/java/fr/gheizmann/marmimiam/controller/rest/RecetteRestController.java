package fr.gheizmann.marmimiam.controller.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gheizmann.marmimiam.dao.CategorieDao;
import fr.gheizmann.marmimiam.dao.IngredientDao;
import fr.gheizmann.marmimiam.dao.IngredientRecetteDao;
import fr.gheizmann.marmimiam.dao.NotationDao;
import fr.gheizmann.marmimiam.dao.RecetteDao;
import fr.gheizmann.marmimiam.dao.UtilisateurDao;
import fr.gheizmann.marmimiam.dto.IngredientRecetteDto;
import fr.gheizmann.marmimiam.dto.NoteDto;
import fr.gheizmann.marmimiam.dto.RecetteDto;
import fr.gheizmann.marmimiam.model.Categorie;
import fr.gheizmann.marmimiam.model.Ingredient;
import fr.gheizmann.marmimiam.model.IngredientRecette;
import fr.gheizmann.marmimiam.model.Notation;
import fr.gheizmann.marmimiam.model.Recette;
import fr.gheizmann.marmimiam.model.user.Utilisateur;

@CrossOrigin
@RestController
public class RecetteRestController {
	private UtilisateurDao utilisateurDao;
	private RecetteDao recetteDao;
	private IngredientDao ingredientDao;
	private IngredientRecetteDao ingredientRecetteDao;
	private CategorieDao categorieDao;
	private NotationDao notationDao;
	private EntityManager entityManager;
	
	@Autowired
	public RecetteRestController(
			final UtilisateurDao utilisateurDao,
			final RecetteDao recetteDao,
			final IngredientDao ingredientDao,
			final IngredientRecetteDao ingredientRecetteDao,
			final CategorieDao categorieDao,
			final NotationDao notationDao,
			final EntityManager entityManager) {
		this.utilisateurDao = utilisateurDao;
		this.recetteDao = recetteDao;
		this.ingredientDao = ingredientDao;
		this.ingredientRecetteDao = ingredientRecetteDao;
		this.categorieDao = categorieDao;
		this.notationDao = notationDao;
		this.entityManager = entityManager;
	}
	
	@PutMapping("/user/recette/nouvelle")
	public ResponseEntity<String> ajouterRecette(@RequestBody RecetteDto recetteDto) {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String username;
		Recette recette;
		Categorie categorie;
		Optional<Categorie> categorieSearch;
		ResponseEntity<String> ret;
		
		if(authentication == null) {
			ret = ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body("Une connexion est requise pour cette opération");
		}
		else {
			username = authentication.getName();
			recette = new Recette();
			recette.setTitre(recetteDto.getTitre());
			recette.setDateMiseEnLigne(new Date(System.currentTimeMillis()));
			recette.setDureePreparation(recetteDto.getDureePreparation());
			recette.setDureeCuisson(recetteDto.getDureeCuisson());
			recette.setDifficulte(recetteDto.getDifficulte());
			recette.setPrix(recetteDto.getPrix());
			recette.setTexte(recetteDto.getTexte());
			recette.setUtilisateur(utilisateurDao.findByLogin(username).get());
			recette.setUrlImage(recetteDto.getUrlImage());
			recette.setActive(true);
			
			categorieSearch = categorieDao.findByNom(recetteDto.getCategorie());
			if(categorieSearch.isPresent()) {
				categorie = categorieSearch.get();
			}
			else {
				categorie = new Categorie();
				categorie.setNom(recetteDto.getCategorie());
				categorie = categorieDao.saveAndFlush(categorie);
			}
			recette.setCategorie(categorie);
			
			recetteDao.saveAndFlush(recette);
			Iterator<IngredientRecetteDto> iterateur = recetteDto.getListeIngredients().iterator();
			while(iterateur.hasNext()) {
				IngredientRecetteDto ingredientLu = iterateur.next();
				Optional<Ingredient> optIngredient = ingredientDao.findByNom(ingredientLu.getNom());
				Ingredient ingredient;
				if(optIngredient.isEmpty()) {
					ingredient = new Ingredient();
					ingredient.setNom(ingredientLu.getNom());
					ingredient = ingredientDao.saveAndFlush(ingredient);
				}
				else {
					ingredient = optIngredient.get();
				}
				IngredientRecette ingredientRecette = new IngredientRecette();
				ingredientRecette.setIngredientId(ingredient.getId());
				ingredientRecette.setRecetteId(recette.getId());
				ingredientRecette.setQuantite(ingredientLu.getQuantite());
				ingredientRecette.setUnite(ingredientLu.getUnite());
				ingredientRecetteDao.saveAndFlush(ingredientRecette);
			}
			ret = ResponseEntity.ok("La recette a bien été créée");
		}
		
		return ret;
	}
	
	@GetMapping("/recette/voir/dernieres/{qte}")
	public List<Recette> getDernieresRecettes(@PathVariable String qte) {
		List<Recette> ret = new ArrayList<>();
		
		int qteInt = 0;
		try {
			qteInt = Integer.parseInt(qte);
			if(qteInt < 0) {
				qteInt = 5;
			}
		}
		catch (NumberFormatException e) {
			qteInt = 5;
		}
		Query query = entityManager.createQuery("from Recette r order by r.id desc");
		ret = (List<Recette>)query.setMaxResults(qteInt).getResultList();
		ret = ret.stream()
				.filter(Recette::isActive).collect(Collectors.toList());
		
		return ret;
	}
	
	@GetMapping("/recette/voir/{id}")
	public Optional<Recette> getRecetteById(@PathVariable String id) {
		Optional<Recette> ret = recetteDao.findById(Integer.parseInt(id));
		
		if(ret.isPresent() && !ret.get().isActive()) {
			ret = Optional.empty();
		}
		
		return ret;
	}
	
	@GetMapping("/recette/voir/user/{utilisateur}")
	public List<Recette> getAllRecetteByUser(@PathVariable String utilisateur) {
		Optional<Utilisateur> optUtilisateur = utilisateurDao.findByLogin(utilisateur);
		List<Recette> ret = new ArrayList<>();
		
		if(optUtilisateur.isPresent()) {
			ret = recetteDao.findByUtilisateurId(optUtilisateur.get().getId())
					.stream()
					.filter(Recette::isActive)
					.collect(Collectors.toList());
		}
		
		return ret;
	}
	
	@PutMapping("/user/recette/supprimer/{id}")
	public ResponseEntity<String> supprimerRecette(@PathVariable Integer id) {
		ResponseEntity<String> ret;
		
		Optional<Recette> optRecette = recetteDao.findById(id);
		if(optRecette.isPresent()) {
			optRecette.get().setActive(false);
			recetteDao.saveAndFlush(optRecette.get());
			
			ret = ResponseEntity.ok("Recette supprimée");
		}
		else {
			ret = ResponseEntity.status(HttpStatus.NOT_FOUND).build().ok("Recette introuvable");
		}
		
		return ret;
	}
	
	@PutMapping("/user/recette/noter/{id}")
	public ResponseEntity<String> noterRecette(@PathVariable Integer id, @RequestBody NoteDto noteDto) throws Exception {
		ResponseEntity<String> ret;
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String username;
		Notation notation;
		
		if(authentication == null) {
			throw new Exception("No user authenticated!");
		}
		username = authentication.getName();
		
		Optional<Recette> optRecette = recetteDao.findById(id);
		Optional<Notation> optNotation = notationDao.findByUtilisateurIdAndRecetteId(utilisateurDao.findByLogin(username).get().getId(), id);
		if(optNotation.isEmpty()) {
			if(optRecette.isPresent() && optRecette.get().isActive()) {
				notation = new Notation();
				notation.setUtilisateur(utilisateurDao.findByLogin(username).get());
				notation.setRecette(optRecette.get());
				notation.setUtilisateurId(utilisateurDao.findByLogin(username).get().getId());
				notation.setRecetteId(optRecette.get().getId());
				notation.setNote(noteDto.getNote());
				notation.setCommentaire(noteDto.getCommentaire());
				notation.setDateNotation(new Date(System.currentTimeMillis()));
				notationDao.save(notation);
				ret = ResponseEntity.ok("Notation de la recette d'id=" + id);
			}
			else {
				ret = ResponseEntity.status(HttpStatus.NOT_FOUND).body("Recette introuvable");
			}
		}
		else
		{
			ret = ResponseEntity.status(HttpStatus.FORBIDDEN).body("Recette déjà notée");
		}
				
		return ret;
	}
	
	@GetMapping("/recette/moyenne/{id}")
	public Double getMoyenneRecette(@PathVariable Integer id) {
		Double ret = null;
		List<Notation> listeNotation = notationDao.findByRecetteId(id);
		Optional<Recette> recette = recetteDao.findById(id);
		
		if(!listeNotation.isEmpty() && recette.isPresent() && recette.get().isActive()) {
			ret = 0.0;
			for(Notation note : listeNotation) {
				ret += (double)note.getNote();
			}
			ret /= (double)listeNotation.size();
		}
		
		return ret;
	}
	
	@GetMapping("/recette/notation/{id}")
	public List<Notation> getNotationRecette(@PathVariable Integer id) {
		Optional<Recette> recette = recetteDao.findById(id);
		List<Notation> ret = new ArrayList<>();
		
		if(recette.isPresent() && recette.get().isActive()) {
			ret = notationDao.findByRecetteId(id);
		}
		
		return ret;
	}
	
	@GetMapping("/user/recette/estdejanotee/{idRecette}")
	public Boolean recetteDejaNotee(@PathVariable Integer idRecette) {
		Boolean ret = false;
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		String username = null;
		Optional<Notation> optNotation;
		
		if(authentication != null) {	
			username = authentication.getName();
			Integer utilisateurId = utilisateurDao.findByLogin(username).get().getId();
			optNotation = notationDao.findByUtilisateurIdAndRecetteId(utilisateurId, idRecette);
			if(optNotation.isPresent()) {
				ret = true;
			}
		}
		
		return ret;
	}
}
