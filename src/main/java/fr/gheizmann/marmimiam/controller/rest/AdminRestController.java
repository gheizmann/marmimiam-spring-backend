package fr.gheizmann.marmimiam.controller.rest;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gheizmann.marmimiam.dao.RoleDao;
import fr.gheizmann.marmimiam.dao.UtilisateurDao;
import fr.gheizmann.marmimiam.model.user.Role;
import fr.gheizmann.marmimiam.model.user.Utilisateur;

@CrossOrigin
@RestController
public class AdminRestController {
	private UtilisateurDao utilisateurDao;
	private RoleDao roleDao;
	private EntityManager entityManager;
	
	@Autowired
	public AdminRestController(
			final UtilisateurDao utilisateurDao,
			final RoleDao roleDao,
			final EntityManager entityManager) {
		this.utilisateurDao = utilisateurDao;
		this.roleDao = roleDao;
		this.entityManager = entityManager;
	}
	
	@GetMapping("/admin/listeroles/{idUtilisateur}")
	public Set<Role> getListeRoles(@PathVariable Integer idUtilisateur) {
		Set<Role> ret = new HashSet<>();
		Optional<Utilisateur> optUtilisateur = utilisateurDao.findById(idUtilisateur);
		
		if(optUtilisateur.isPresent()) {
			ret = optUtilisateur.get().getListeRole();
		}
		
		return ret;
	}
	
	@PutMapping("/admin/setlisteroles/{idUtilisateur}")
	public ResponseEntity<String> setListeRoles(@PathVariable Integer idUtilisateur, @RequestBody List<String> listeRoles) {
		ResponseEntity<String> ret;
		Optional<Utilisateur> optUtilisateur = utilisateurDao.findById(idUtilisateur);
		boolean rolesOk = true;
		Set<Role> setRoles = new HashSet<>();
		Role role;
		
		Iterator<String> it = listeRoles.iterator();
		while(it.hasNext()) {
			String roleStr = it.next();
			switch(roleStr) {
			case "user":
				role = roleDao.findByNom("ROLE_USER").get();
				setRoles.add(role);
				break;
			case "admin":
				role = roleDao.findByNom("ROLE_ADMIN").get();
				setRoles.add(role);
				break;
			default:
				rolesOk = false;
			}
			if(!rolesOk) {
				break;
			}
		}
		
		if(optUtilisateur.isPresent() && rolesOk) {
			Utilisateur utilisateur = optUtilisateur.get();
			utilisateur.setListeRole(setRoles);
			utilisateurDao.saveAndFlush(utilisateur);
			ret = ResponseEntity.ok("L'utilisateur " + utilisateur.getLogin() + " a eu ses rôles changés !");
		}
		else {
			ret = ResponseEntity.badRequest().body("Mauvaise requête");
		}
		
		return ret;
	}
	
	@GetMapping("/admin/listeusers/{noPage}")
	public List<Utilisateur> getListUsers(@PathVariable Integer noPage) {
		List<Utilisateur> listUsers = null;
		int noRow = (noPage-1)*5;
		if(noRow >= 0) {
			Query query = entityManager.createQuery("from Utilisateur u");
			listUsers = query.setFirstResult(noRow).setMaxResults(5).getResultList();
		}
		
		return listUsers;
	}
}
